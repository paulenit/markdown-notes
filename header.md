---
author: Author Name
title: "Title"
subtitle: "Subtitle"
date: \today
documentclass: scrartcl
classoption:
  - a4paper
  - 11pt
#   - twocolumn
fontfamily: libertinus
# Do not use libertinus monospace font
fontfamilyoptions:
  - tt=false
csquotes: true
# Use Inconsolata as monospace font, same as in ACM template
# Use Libertine math font, works with Libertinus
header-includes:
- |
  ```{=latex}
  \usepackage[varqu, scaled=.93]{inconsolata}
  \usepackage[libertine]{newtxmath}
  ```
toc: yes
numbersections: true
# bibliography: "path/to/bibliography"
link-citations: true
---
<!-- This document is written in Pandoc Markdown (https://pandoc.org/MANUAL.html#pandocs-markdown) -->