# Markdown Notes

This is my template for my note-taking workflow.
I write the notes in [Pandoc Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown) and render them using LaTeX.

This repository contains my [Pandoc Markdown YAML metadata block](https://pandoc.org/MANUAL.html#extension-yaml_metadata_block) to configure document metadata, fonts etc. as well as a [Makefile](https://www.gnu.org/software/make/) to render the document to PDF.

It also includes two [VSCode](https://code.visualstudio.com/) build tasks to build the files from within the editor.

## The Header

The document uses the [Libertinus fonts](https://github.com/alerque/libertinus) with [Inconsolata](https://levien.com/type/myfonts/inconsolata.html) for monospace text.
These are the fonts also used in the [ACM article template](https://www.ctan.org/pkg/acmart).

## The Makefile

Pandoc can compile directly from Markdown to PDF (which still converts to .tex internally).
The makefile uses [Pandoc](https://pandoc.org/index.html) to convert to LaTeX and then render the document with [latexmk](https://www.ctan.org/pkg/latexmk/) to optimize the number of compilation runs.

The intermediate files are stored in the `.build` directory.

The `latexmkrc` file in this repository contains configuration for latexmk.
Use of this file enables automatic conversion of SVG graphics included in Markdown files.

## The Build Tasks

The .vscode folder contains two build tasks to convert documents from Markdown to PDF in the VSCode editor. The `pandoc md -> pdf` task converts directly from Markdown to PDF without producing intermediate LaTeX files. The `GNU make (default recipe)` executes the default make recipe in the folder of the currently opened file.