# The default recipe will render a PDF from the markdown source using LaTeX.
# It will create and write intermediate files to .build.
# This will reduce compilation time as latexmk is used to avoid unnecessary runs.

NOTES_NAME := notes

# Compile tex file to pdf using latexmk
$(NOTES_NAME).pdf: .build/$(NOTES_NAME).tex
	latexmk -pdf $< -outdir=.build
	cp .build/$@ $@

# Convert markdown to latex with citations. Use .build folder
.build/$(NOTES_NAME).tex: $(NOTES_NAME).md | .build
	pandoc --standalone --to=latex --biblatex --output=./.build/$(basename $<).tex --resource-path=. $<

# Ensure the .build folder exists
.build:
	mkdir -p $@

clean:
	rm -r .build

all: $(NOTES_NAME).pdf
