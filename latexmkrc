$pdf_mode = 1;

# Clean up pdf files generated from svg files
$cleanup_includes_cusdep_generated = 1;

# Always regenerate .bbl files from .bib files and clean them
$bibtex_use = 2;

# Additional generated files for cleanup
push @generated_exts, 'glo', 'glsdefs', 'xdy';

# Convert svg to pdf on the fly using inkscape
add_cus_dep('svg', 'pdf', 0, 'svg2pdf');
sub svg2pdf {
    system("inkscape -D $_[0].svg -o $_[0].pdf");
}

# Convert drawio to pdf on the fly using draw.io-desktop
add_cus_dep('drawio', 'pdf', 0, 'drawio2pdf');
sub drawio2pdf {
    system("drawio --export --format pdf --output $_[0].pdf $_[0].drawio");
}